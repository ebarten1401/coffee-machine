package com.elsa.coffeestore.presentation;

import com.elsa.coffeestore.machines.CommercialMachine;
import com.elsa.coffeestore.machines.Machine;
import com.elsa.coffeestore.machines.PodMachine;
import com.elsa.coffeestore.machines.SemiCommercialMachine;
import com.elsa.coffeestore.pods.LongShotPod;
import com.elsa.coffeestore.pods.Pod;
import com.elsa.coffeestore.pods.StandardShotPod;
import com.elsa.onceAndOnlyOnce.CollectionHelper;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    private List<Machine> machines;
    private List<Pod> pods;

    {
        PodMachine longShotPodMachine = new PodMachine<LongShotPod>();
        longShotPodMachine.setPod(new LongShotPod());

        PodMachine standardShotPodPodMachine = new PodMachine<StandardShotPod>();
        standardShotPodPodMachine.setPod(new StandardShotPod());

        machines = (List<Machine>) CollectionHelper.Create(new ArrayList<>(), new CommercialMachine(),
                new SemiCommercialMachine(), longShotPodMachine, standardShotPodPodMachine);

        pods = (List<Pod>) CollectionHelper.Create(new ArrayList<>(), new StandardShotPod(), new LongShotPod());
    }

    public List<Machine> getAllMachines() {
        return machines;
    }

    public void addMachine(Machine item) {
        machines.add(item);
    }

    public List<Pod> getAllPods() {
        return pods;
    }
}
