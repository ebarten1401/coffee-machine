package com.elsa.coffeestore.presentation;

public class Presentation {
    private Inventory inventory = new Inventory();

    public void DisplayMachines() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("**\tMACHINES\t**\n");

        inventory.getAllMachines().stream().forEach(item -> strBuilder.append(String.format("**\t%s;\t**\n", item.makeCoffee())));
        System.out.println(strBuilder.toString());
    }
}
