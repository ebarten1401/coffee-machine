package com.elsa.coffeestore.machines;

public class SemiCommercialMachine extends Machine {
    public String makeCoffee() {
        return "Semi Commercial Machine";
    }
}
