package com.elsa.coffeestore.machines;

import com.elsa.coffeestore.pods.Pod;

public class PodMachine<T extends Pod> extends Machine {
    private T pod;

    public T getPod() {
        return pod;
    }

    public void setPod(T pod) {
        this.pod = pod;
    }

    public String makeCoffee() {
        return pod.getType() + "Coffee";
    }

}
