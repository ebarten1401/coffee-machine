package com.elsa.coffeestore.machines;

public class CommercialMachine extends Machine {

    public String makeCoffee() {
        return "Making commercial coffee";
    }
}
