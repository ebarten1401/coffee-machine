package com.elsa.coffeestore.machines;

public abstract class Machine {
    public abstract String makeCoffee();

    public String printTest() {
        return "Test Output from Machine";
    }

}
