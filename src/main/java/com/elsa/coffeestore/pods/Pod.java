package com.elsa.coffeestore.pods;

public abstract class Pod {

    public static final int EMPTY = 0;
    public static final int SHOT_SIZE = 30;

    protected long unusedCoffee = EMPTY;

    /**
     * The method returns the amount of unused grams of coffee in the pod.
     *
     * @return The amount of unused grams of coffee in the pod.
     */
    public long getUnusedCoffee() {
        return unusedCoffee;
    }

    public PodType getType() {
        return null;
    }

    /**
     * The method returns true if the pod is empty of coffee. false - otherwise.
     *
     * @return true if the pod is empty of coffee. false - otherwise.
     */
    public boolean isEmpty() {
        return unusedCoffee == EMPTY;
    }

    public void extract() {
        unusedCoffee = -SHOT_SIZE;
    }
}
