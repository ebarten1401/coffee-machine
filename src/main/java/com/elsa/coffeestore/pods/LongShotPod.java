package com.elsa.coffeestore.pods;

public class LongShotPod extends Pod {

    public LongShotPod() {
        unusedCoffee = SHOT_SIZE * 2;
    }

    @Override
    public PodType getType() {
        return PodType.Long;
    }
}
