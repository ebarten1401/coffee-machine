package com.elsa.coffeestore.pods;

public class StandardShotPod extends Pod {

    public StandardShotPod() {
        unusedCoffee = 30;
    }

    @Override
    public PodType getType() {
        return PodType.Standard;
    }
}
