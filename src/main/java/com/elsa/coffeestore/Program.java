package com.elsa.coffeestore;

import com.elsa.coffeestore.presentation.Presentation;

import java.util.Scanner;

public class Program {

    private static String displayMenu = null;

    public static void main(String[] args) {
        Presentation presentation = new Presentation();

        int input = 0;
        while (input != -1) {
            input = getInput();
            switch (input) {
                case 0:
                    presentation.DisplayMachines();
                    break;

            }
        }


    }

    private static int getInput() {
        if (displayMenu == null) {
            displayMenu = getMenu();
        }

        while (true) {
            try {
                System.out.println(displayMenu);
                Scanner in = new Scanner(System.in);
                return Integer.parseInt(in.nextLine());
            } catch (Exception e) {
                // Too lazy to validate input.
                System.out.println("Invalid input!");

            }
        }

    }

    private static String getMenu() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(0) Show Machines\n");
        stringBuilder.append("(1) Show Pods\n");
        stringBuilder.append("(-1) Exit\n");
        return stringBuilder.toString();
    }
}