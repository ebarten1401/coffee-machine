package com.elsa.onceAndOnlyOnce;

import java.util.Arrays;
import java.util.Collection;

public class CollectionHelper {
    public static <T> Collection<T> Create(Collection<T> c, T... items) {
        c.addAll(Arrays.asList(items));
        return c;
    }
}
